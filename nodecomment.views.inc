<?php
/**
 * @file
 * Provide views data and handlers for nodecomment.module
 */

/**
 * @defgroup views_nodecomment_module nodecomment.module handlers
 *
 * Includes the tables 'node', 'node_comments' and 'node_comment_statistics'.
 * @{
 */

/**
 * Implementation of hook_views_data()
 */
function nodecomment_views_data() {
  // since comments are nodes we keep with the Node group
  $data['node_comments']['table']['group']  = t('Node');
	
	// ----------------------------------------------------------------
  // node table -- basic table information.

	$data['node_comments']['table']['join'] = array(
		// node_comments links to node
		'node' => array(
			'left_field' => 'nid',
			'field' => 'cid',
		),
	);
	
	// ----------------------------------------------------------------
  // node_comments table -- fields
  
  $data['node_comments']['cid'] = array(
    'title' => t('Nid'),
    'help' => t('The node ID of the comment.'),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title', 
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['node_comments']['nid'] = array(
    'title' => t('Original Parent Nid'),
    'help' => t('The original node the comment is a reply to.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
  );
  
  $data['node_comments']['pid'] = array(
    'title' => t('Parent Nid'),
    'help' => t('The node of the parent comment. Could be another comment.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'relationship' => array(
      'title' => t('Parent comment'),
      'help' => t('The parent comment.'),
      'base' => 'comments',
      'field' => 'cid',
      'handler' => 'views_handler_relationship',
      'label' => t('Parent comment'),
    ),
  );
  
  $data['node_comments']['name'] = array(
    'title' => t('Author'),
    'help' => t('The name of the poster.'),
    'field' => array(
      'handler' => 'views_handler_field_username_comment',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['node_comments']['homepage'] = array(
    'title' => t("Author's website"),
    'help' => t("The website address of the comment's author. Can be a link. The homepage can also be linked with the Name field. Will be empty if posted by a registered user."),
    'field' => array(
      'handler' => 'views_handler_field_url',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['node_comments']['thread'] = array(
    'field' => array(
      'title' => t('Depth'),
      'help' => t('Display the depth of the comment if it is threaded.'),
      'handler' => 'views_handler_field_comment_depth',
    ),
    'sort' => array(
      'title' => t('Thread'),
      'help' => t('Sort by the threaded order. This will keep child comments together with their parents.'),
      'handler' => 'views_handler_sort_comment_thread',
    ),
  );
  
  // link to reply to comment
  $data['node_comments']['replyto_comment'] = array(
    'field' => array(
      'title' => t('Reply-to link'),
      'help' => t('Provide a simple link to reply to the comment.'),
      'handler' => 'views_handler_field_comment_link_reply',
    ),
  );
	
  return $data;
}

/**
 * Field handler to allow linking to a user account or homepage
 */
class views_handler_field_username_comment extends views_handler_field {
  /**
   * Override init function to add uid and homepage fields.
   */
  function init(&$view, &$data) {
    parent::init($view, $data);
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['homepage'] = 'homepage';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_user'] = array('default' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_user'] = array(
      '#title' => t("Link this field to its user or an author's homepage"),
      '#type' => 'checkbox',
      '#default_value' => $this->options['link_to_user'],
    );
  }

  function render_link($data, $values) {
    $account->uid = $values->{$this->aliases['uid']};
    $account->name = $values->{$this->field_alias};
    $account->homepage = $values->{$this->aliases['homepage']};

    if (!empty($this->options['link_to_user'])) {
      return theme('username', $account);
    }
    else {
      return $data;
    }
  }

  function render($values) {
    return $this->render_link(check_plain($values->{$this->field_alias}), $values);
  }

}

/**
 * Field handler to display the depth of a comment
 */
class views_handler_field_comment_depth extends views_handler_field {
  /**
   * Work out the depth of this comment
   */
  function render($values) {
    return count(explode('.', $values->comments_thread)) - 1;
  }
}

/**
 * Sort handler for ordering by thread
 */
class views_handler_sort_comment_thread extends views_handler_sort {
  function query() {
    $this->ensure_my_table();

    //Read comment_render() in comment.module for an explanation of the
    //thinking behind this sort.
    if ($this->options['order'] == 'DESC') {
      $this->query->add_orderby($this->table_alias, $this->real_field, $this->options['order']);
    }
    else {
      $alias = $this->table_alias . '_' . $this->real_field . 'asc';
      //@todo is this secure?
      $this->query->add_orderby(NULL, "SUBSTRING({$this->table_alias}.{$this->real_field}, 1, (LENGTH({$this->table_alias}.{$this->real_field}) - 1))", $this->options['order'], $alias);
    }
  }
}

/**
 * Base field handler to present a link.
 */
class views_handler_field_comment_link extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['cid'] = 'cid';
    $this->additional_fields['nid'] = 'nid';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('view');
    return l($text, "node/" . $values->{$this->aliases['nid']}, array('html' => TRUE, 'fragment' => "comment-" . $values->{$this->aliases['cid']}));
  }
}

/**
 * Field handler to present a link to delete a node.
 */
class views_handler_field_comment_link_reply extends views_handler_field_comment_link {
  function render($values) {
    //check for permission to reply to comments
    if (!user_access('post comments')) {
      return;
    }
    $text = !empty($this->options['text']) ? $this->options['text'] : t('reply');
    return l($text, "node/add/comment/" . $values->{$this->aliases['nid']} . '/' . $values->{$this->aliases['cid']});
  }
}

/**
 * Implementation of hook_views_plugins
 */
function nodecomment_views_plugins() {
  return array(
    'style' => array(
      'nodecomment_threaded' => array(
        'title' => t('Comments threaded'),
        'help' => t('Display the comment with a threaded comment view.'),
        'handler' => 'nodecomment_plugin_style_threaded',
        'theme' => 'nodecomment_threaded',
        'uses row plugin' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}

class nodecomment_plugin_style_threaded extends views_plugin_style {
  function render() {
    $divs = 0;
    $last_depth = 0;
    drupal_add_css(drupal_get_path('module', 'comment') .'/comment.css');

    foreach ($this->view->result as $n) {
      $node = node_load($n->nid);
      $node->depth = count(explode('.', $node->thread)) - 1;

      if ($node->depth > $last_depth) {
        $divs++;
        $output .= '<div class="indented">';
        $last_depth++;
      }
      else {
        while ($node->depth < $last_depth) {
          $divs--;
          $output .= '</div>';
          $last_depth--;
        }
      }
      $output .= node_view($node);
    }

    for ($i = 0; $i < $divs; $i++) {
      $output .= '</div>';
    }

    return $output;
  }
}

